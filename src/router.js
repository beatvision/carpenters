import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/home.vue')
    },
    {
      path: '/create',
      name: 'create',
      component: () => import('./views/create.vue')
    },
    {
      path: '/edit/:Pid',
      name: 'edit',
      component: () => import('./views/edit.vue')
    }
  ]
})
